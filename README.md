# SPIIR Analytics Dashboard

Streamsync application to analyze SPIIR pipeline trigger data.

## Quick Start

### Virtual Environment

We use `conda`/`mamba` to prepare a virtal environment to run this project. The default name of the environment is "spiir-analytics" as defined in the environment.yml file, but this can be edited with the `-n <name>` parameter.

```bash
mamba env create -f environment.yml
```

Once created, the virtual environment can be activated before running and/or editing the dashboard application.

```bash
mamba activate spiir-analytics
```

### Run Dashboard Application

We can call the `run` command from the `streamsync` package and point it at our dashboard application folder, `app/`, in order to deploy the dashboard for viewing.

```bash
streamsync run app
```

### Edit Dashboard Application

Likewise, we can run the Streamsync Builder (a code and UI editor) by running the following command:

```bash
streamsync edit app
```

### Environment Variables

Currently, we use environment variables to specify the default directory to load coinc.xml trigger files from. Note that this file path is currently configured to be relative to the `app/` directory, so for example if `app/` and `coinc_dir/` are in the same root location, we would specify `COINC_DIR="../coinc_dir/"`.

The default is currently defined as `COINC_DIR="../share/spiir/MDC/"` in the Settings of the `app/main.py` file.

However, we can change the default trigger directory by editing the environment variable as follows:

- Setting the environment variable when instantiating the application, i.e:

  ```
    COINC_DIR="../data/spiir/ER15/" streamsync run app
  ```

- Creating an .env file in the location directory with a line:

  ```
    COINC_DIR="../data/spiir/ER15/"
  ```

Alternatively, when the application starts up, the user can simply edit the directory text input field on the left sidebar, and the application will automatically discover files in that location.
