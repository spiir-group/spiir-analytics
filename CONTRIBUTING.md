# Contribution Guide

This section details some considerations for contributors to the repository.

## Git Commit Hooks

We use [pre-commit](https://pre-commit.com/) as part of development to ensure all commits are up to the linting
standards configured for the repository. To initialize pre-commit, run the following after activating your virtual environment:

```
pre-commit install
```

This should enable pre-commit to automatically trigger and try to fix errors every time a git commit is made, but only on the files involved in the commit itself.

To run pre-commit and fix errors on all files in the repository, run the following:

```
pre-commit run --all-files
```
