"""Entry point script for the SPIIR analytics dashboard application."""

from io import BytesIO
from pathlib import Path
from typing import Iterable

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import spiir.io
import streamsync as ss
import yaml
from dotenv import load_dotenv
from pydantic import BaseSettings
from streamsync.core import StreamsyncSession, StreamsyncState

load_dotenv()  # take environment variables from .env.


class Settings(BaseSettings):
    """Environment variable settings for the dashboard application.

    Parameters:
        coinc_dir (str): Path to the folder that contains our coinc files.
        coinc_ext (str): File extension for coinc files, default = '.xml'.
    """

    coinc_dir: str = "../share/spiir/MDC"  # relative to app/ directory
    coinc_ext: str = ".xml"  # TODO: enable multiple extensions (.xml, .xml.gz)

    # skymap_dir: str | None
    # p_astro_dir: str | None


SETTINGS = Settings()

# TODO: Merge CONFIG (file) and SETTINGS (env vars)
try:
    with open("./config.yaml", "r") as f:  # relative to app/ directory
        CONFIG = yaml.safe_load(f)
except Exception:
    CONFIG = {}

##################
## Data Loaders ##
##################


def _load_snr_figure(state: StreamsyncState):
    """Plots the SNR figure from the loaded coinc.xml file data."""
    file_name = Path(state["loaded_file"]).name
    network_snrs = state["snr_df"]
    fig, ax = plt.subplots(figsize=(16, 7))
    network_snrs.plot(title=file_name, xlabel="Time", ylabel="SNR", ax=ax)
    ax.grid(which="both", axis="both", alpha=0.5)
    state["snr_fig"] = fig


def _load_psd_figure(state: StreamsyncState):
    """Plots the PSD figure from the loaded coinc.xml file data."""
    psds = state["psd_df"]
    file_name = Path(state["loaded_file"]).name
    fig, ax = plt.subplots(figsize=(16, 7))
    psds.plot(ax=ax, logx=True, logy=True)
    xlim = (1, psds.index.max() + 10)
    ax.set(xlim=xlim, title=file_name, xlabel="Frequency", ylabel="Amplitude")
    ax.grid(which="both", axis="both", alpha=0.5)
    state["psd_fig"] = fig


def _load_coinc_xml_data(state: StreamsyncState):
    """Loads SPIIR data from the currently selected coinc.xml file."""
    state["loaded_file"] = state["selected_file"]
    data = spiir.io.ligolw.load_coinc_xml(state["loaded_file"])

    # run pre-processing and cleaning for postcoh table
    postcoh = data["tables"]["postcoh"][CONFIG["tables"]["postcoh"]["columns"]]
    postcoh.index = [Path(state["loaded_file"]).name]
    postcoh = postcoh.replace([np.inf, -np.inf], np.nan).replace(np.nan, "-").T
    state["postcoh_df"]: pd.DataFrame = postcoh

    # concatenate PSD and network SNR arrays together across all interferometers
    snrs = pd.concat(data["snrs"], axis=1)
    network_snrs = snrs.apply(
        lambda x: np.sqrt(np.real(x) ** 2 + np.imag(x) ** 2), axis=0
    )
    state["snr_df"] = network_snrs  # NOTE: streamsync cant serialize complex df
    state["psd_df"]: pd.DataFrame = pd.concat(data["psds"], axis=1)

    _load_psd_figure(state)
    _load_snr_figure(state)
    state["has_loaded_file"] = True


####################
## State Updaters ##
####################


def _update_files(state: StreamsyncState):
    """Updates the list of discovered data files in the application state."""
    extension = SETTINGS.coinc_ext  # get file extension, e.g. ".xml"
    paths: Iterable[Path] = Path(state["coinc_dir"]).glob(f"*{extension}")
    state["files"] = {path.stem: path.name for path in paths}


def update(state: StreamsyncState, session: StreamsyncSession | None = None):
    """Updates the application state."""
    _update_files(state)


####################
## Event Handlers ##
####################


def handle_select_file(state: StreamsyncState, payload: str):
    """Handler function when a file select component is interacted with."""
    extension = SETTINGS.coinc_ext  # get file extension, e.g. ".xml"
    file_path = Path(state["coinc_dir"]) / f"{payload}{extension}"
    state["selected_file"] = str(file_path)
    state["has_selected_file"] = True
    update(state)


def handle_reset_file(state: StreamsyncState):
    """Handler function for resetting any selected or loaded file components."""
    # reset sidebar selections
    state["coinc_dir"] = SETTINGS.coinc_dir
    state["radio_input_select_key"] = None
    state["selected_file"] = None
    state["has_selected_file"] = None

    handle_unload_file(state)


def handle_unload_file(state: StreamsyncState):
    """Handler function for unloading loaded file components."""
    state["loaded_file"] = None
    state["has_loaded_file"] = False

    update(state)


def handle_load_file(state: StreamsyncState):
    """Handler function when a file load component is interacted with."""
    if state["selected_file"] is not None:
        _load_coinc_xml_data(state)  # TODO: don't reload if correct file already loaded
        update(state)


def handle_download_selected_file(state: StreamsyncState):
    """Handler function when a file download component is interacted with."""
    if state["selected_file"] is not None:
        data = ss.pack_file(state["selected_file"], "text/plain")
        file_name = Path(state["selected_file"]).name
        state.file_download(data, file_name)


def handle_download_loaded_file(state: StreamsyncState):
    """Handler function when a file download component is interacted with."""
    if state["loaded_file"] is not None:
        data = ss.pack_file(state["loaded_file"], "text/plain")
        file_name = Path(state["loaded_file"]).name
        state.file_download(data, file_name)


def handle_download_postcoh_csv(state: StreamsyncState):
    """Handler function when a postcoh table download component is interacted with."""
    if state["postcoh_df"] is not None:
        content = state["postcoh_df"].to_csv(lineterminator='\r\n')
        data = ss.pack_bytes(bytes(content, encoding="utf-8"))
        file_name = f"{Path(state['loaded_file']).stem}_postcoh.csv"
        state.file_download(data, file_name)


def handle_download_psd_fig(state: StreamsyncState):
    """Handler function when a PSD figure download component is interacted with."""
    if state["psd_fig"] is not None:
        with BytesIO() as buffer:  # use buffer memory
            state["psd_fig"].savefig(buffer, format='png')
            buffer.seek(0)
            data = ss.pack_bytes(buffer.getvalue())
        file_name = f"{Path(state['loaded_file']).stem}_PSD.png"
        state.file_download(data, file_name)


def handle_download_snr_fig(state: StreamsyncState):
    """Handler function when a SNR figure download component is interacted with."""
    if state["snr_fig"] is not None:
        with BytesIO() as buffer:  # use buffer memory
            state["snr_fig"].savefig(buffer, format='png')
            buffer.seek(0)
            data = ss.pack_bytes(buffer.getvalue())
        file_name = f"{Path(state['loaded_file']).stem}_SNR.png"
        state.file_download(data, file_name)


# Initialise the dashboard application state
initial_state = ss.init_state(
    {
        "my_app": {"title": "SPIIR Analytics Dashboard"},
        "coinc_dir": SETTINGS.coinc_dir,
        "files": {},
        "loaded_file": None,
        "has_loaded_file": False,
        "selected_file": None,
        "has_selected_file": False,
        "radio_input_select_key": None,
    }
)

update(initial_state)
